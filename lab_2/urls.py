from lab_2.models import Note
from django.urls import path
from .views import index,xml,json
from .models import Note

urlpatterns = [
  path('', index, name='index'),
  path('xml/', xml, name='xml'),
  path('json/', json, name='json'),
]
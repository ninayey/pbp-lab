from django.urls import path
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note, name='noteFrom'),
    path('note-list/', note_list, name='noteList'),
]
from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_2.models import Note 
from .forms import NoteForm


def index(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    context ={}
  
    form = NoteForm(request.POST or None)
      
    if (form.is_valid() and request.method=='POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')
  
    context['noteForm']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_note_list.html', response)
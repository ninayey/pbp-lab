from django.shortcuts import render
from lab_2.models import Note 

def index(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)


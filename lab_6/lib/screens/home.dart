import 'package:flutter/material.dart';

import 'faq_screen.dart';


class HomeScreen extends StatelessWidget {
  static const routeName = '/home';

  @override
  Widget build(BuildContext context) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(child: Container(),),
        new Text(
          "Welcome in Hachoo! \n",
          softWrap: true,
          style: new TextStyle(
              fontSize: 25.0,
              fontWeight: FontWeight.bold,
              color: Color(0xff374ABE)),
        ),

        new Container(
          height: 50.0,
          margin: EdgeInsets.all(10),
          child: Center(
            child: RaisedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FaqScreen()),
                );
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40.0)),
              padding: EdgeInsets.all(0.0),
              child: Ink(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                    ),
                    borderRadius: BorderRadius.circular(30.0)),
                child: Container(
                  constraints:
                  BoxConstraints(maxWidth: 300.0, minHeight: 70.0),
                  alignment: Alignment.center,
                  child: Text(
                    "FAQ HACHOO",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
            ),
          ),
        ),

        new Expanded(
          child: Container(),
        ),
      ],
    );

  }
}

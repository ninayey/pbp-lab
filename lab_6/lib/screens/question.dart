import 'package:flutter/material.dart';

import '../models/faq_question.dart';
const dummy = const [
  question(
      Question: "Apa itu vaksin?",
      answer: "Vaksin adalah produk biologi yang berisi antigen berupa bagian mikroorganisme atau zat yang dihasilkannya yang telah diolah sedemikian rupa sehingga aman, yang apabila diberikan kepada seseorang akan menimbulkan kekebalan spesifik secara aktif terhadap penyakit tertentu."
  ),
  question(
      Question: "Apa yang dimaksud vaksinasi?",
      answer: "Vaksinasi adalah proses di dalam tubuh, dimana seseorang menjadi kebal atau terlindungi dari suatu penyakit sehingga apabila suatu saat terpapar penyakit tersebut maka tidak akan sakit atau hanya mengalami sakit ringan, biasanya dengan pemberian vaksin."),
];
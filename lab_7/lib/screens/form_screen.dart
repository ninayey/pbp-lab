import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/faq_screen.dart';

class FormScreen extends StatelessWidget {
  static const routeName = '/form';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(child: Column(
          children: <Widget>[
            Center(
                child: Padding(
                    padding: EdgeInsets.all(35.0),
                    child: Text(
                      "Ask Question!",
                      style: TextStyle(
                        fontSize: 30.0,
                        color: Color(0xff0c0c0c),
                        fontWeight: FontWeight.bold,
                      ),
                    )
                )
            ),

            new Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsetsDirectional.only(top: 25.0, bottom: 5.0, start: 16.0, end: 16.0),
                child: Text(
                  "Pertanyaan:",
                  softWrap: true,
                  style: new TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ),
            ),

            Center(
              child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
            children: <Widget>[
            TextField(
            onChanged: (String value) async {
            if (value != 'tidak') {
            return;
            }
            await showDialog<void>(
            context: context,
            builder: (BuildContext context) {
            return AlertDialog(
            title: const Text('WARNING!'),
            content: const Text('Masukan pertanyan dengan niat!.'),
            actions: <Widget>[
            TextButton(
            onPressed: () {
            Navigator.pop(context);
            },
            child: const Text('OK'),
            ),
            ],
            );
            },
            );
            },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white70,
                        hintText: "Tuliskan pertanyaan Anda...",
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 1, color: Colors.blueAccent),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                    )
                  ],
                ),

              ),
            ),

            SizedBox(height: 20.0),
            MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                padding: EdgeInsets.all(10.0),
                color: Color(0xff07e5f5),
                onPressed: () {
                  _navigateToNextScreen(context);
                },
                child: Text("Kirimkan pertanyaan", style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 17.0
                ))
            )

          ],
        )
        ));
  }
  void _navigateToNextScreen(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => FaqScreen()));
  }
}

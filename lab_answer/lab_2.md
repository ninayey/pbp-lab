1. Apakah perbedaan antara JSON dan XML?

*Definisi*

 XML merupakan singkatan dari Extensible markup language. Kegunaanya untuk membuat halaman web dan aplikasi web. XML merupakan Salah satu bahasa markup yang mendefinisikan seperangkat aturan untuk menyanjikan dokumen dalam format yang dapat dibaca oleh mesin dan manusia sekaligus. Tujuan desain XML sendiri lebih fokus pada kesederhanaan, umum, dan kegunaannya. Jadi, XML adalah format data tekstual dengan dukungan kuat melalui Unicode untuk bahasa manusia yang berbeda. Sedangkan, JSON (JavaScript Object Notation) adalah format pertukaran data yang ringan dan sepenuhnya tidak bergantung pada bahasa. Ini didasarkan pada bahasa pemrograman JavaScript dan mudah dimengerti dan dihasilkan.

----Perbedaan antara JSON dan XML----

1. JSON (JavaScript Object Notation)

- Merupakan JavaScript Object Notation
- Didasarkan pada bahasa JavaScript
- Cara untuk merepresentasikan suatu objek
- Tidak mendukung penggunaan namspaces, tetapi mendukung penggunaan array.
- Relatif mudah untuk di baca 
- Tidak menggunakan tag end dan comment
- Hanya mendukung pengkodean untuk UTF-8

Contoh: 

{"Friends":[
    { "firstName":"Chou", "lastName":"Tzuyu" },
    { "firstName":"Im", "lastName":"Nayeon" },
    { "firstName":"Tempest", "lastName":"Jura" },
    { "firstName":"Gojo", "lastName":"Satoru" }
]}

2. XML (Extensible markup language) 

- Bahasa markup yang dapat diperluas dengan tag yang disesuakai dengan kebutuhan pengguna dan struktur tagnya sendiri untuk mewakili item data
- Berasal dari SGML (Standard Generalized Markup Language )
- Mendukung penggunaan namespaces dan tidak mendukung penggunaan array (bisa tapi agak ribet)
- Mempunyai tag start dan end.
- Mendukung berbagai pengkodean (tidak bergantung hanya pada satu pengkodean)

Contoh: 

<note>
  <to>TWICE</to>
  <from>Nina</from>
  <heading>Welcome to the world</heading>
  <body>I am gonna come to the concert!</body>
</note>


2. Apakah perbedaan antara HTML dan XML?

*Definisi*

HTML singkatan dari Hyper Text Markup Language. Secara harfiah Hyper Text didefinisikan sebagai link antara halaman web dan salah satu bahasa markup, yaitu dokumen teks dalam tag yang mendefinisikan struktur halaman web. Penggunaan dari HTML biasanya untuk membuat halaman statis pada halaman web atau web app. Sedangkan, XML singkatan dari Extensible markup language. XML sama seperti HTML, yaitu penggunaanya untuk membuat halaman web dan aplikasi web. XML adalah salah satu bahasa markup yang mendefinisikan seperangkat aturan untuk menyanjikan dokumen dalam format yang dapat dibaca manusia dan mesin. Keduannya memang memiliki kemiripan, tetapi terdapat beberapa perbedaaan antar keduanya.

----Perbedaan antara HTML dan XML----

1. XML (Extensible markup language) 

- XML adalah singkatan dari eXtensible Markup Language.
- Dirancang untuk membawa data, bukan untuk menampilkan data (HTML).
- Bahasa markup standar/kerangka kerja yang mendefinisikan bahasa markup lainnya.
- XML dirancang untuk menyimpan dan mengangkut data. Jadi, XML membawa data ke dan dari database.
- Tag XML digunakan untuk mendeskripsikan data bukan perintah ditampilkan.
- XML dirancang untuk mendeskripsikan diri sendiri. Tag XML dapat diperluas sehingga Tag didefinisikan sesuai kebutuhan programmer, XML fleksibel karena tag dapat ditentukan bila diperlukan
- XML mendefinisikan cara standar untuk menambahkan markup ke dokumen.
- Menyediakan kemampuan untuk mendefinisikan tag dan hubungan struktural di antara mereka.
- XML tidak mengizinkan terdapat kesalahan
- Dalam XML, tag penutup wajib karena diperlukan.
- Semua semantik dokumen XML akan ditentukan oleh aplikasi yang memprosesnya atau oleh lembar gaya.

Contoh: 
<note>
  <to>TWICE</to>
  <from>Nina</from>
  <heading>Welcome to the world</heading>
  <body>I am gonna come to the concert!</body>
</note>

2.  HTML (Hyper Text Markup Language)

- Digunakan untuk mendesain tata letak dokumen dan untuk menentukan hyperlink.
- HTML adalah singkatan dari HyperText Markup Language.
- Tidak dapat menggunakan whitespaces dalam kode
- Memberi tahu browser bagimana cara menampilkan multimedia seperti teks, gambar, dan media pendukung lainnya dan fitur tata letak halaman baru.
- Menyediakan banyak tag untuk mengontrol penyajian informasi di halaman web, seperti <body>, <li>, <hr> dll. Netapi, Jumlah tag dalam HTML terbatas.
- Tidak case sensitive 
- Tag HTML adalah tag yang telah ditentukan sebelumnya.
- HTML udah diintegrasikan dengan bahasa pemrograman lainnya
- HTML terdiri dari serangkaian elemen dan atribut. 
- HTML dapat mengabaikan kesalahan kecil.
- Dalam HTML, tag penutup tidak selalu diperlukan. 

Contoh: 
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <h1>WELCOME</h1>
        <p>This is Nina.</p>
    </body>
</html>

Referensi:
[1] https://www.upgrad.com/blog/html-vs-xml/#:~:text=HTML%20and%20XML%20are%20related,language%20that%20defines%20other%20languages.
[2] https://www.guru99.com/xml-vs-html-difference.html
[3] https://www.geeksforgeeks.org/html-vs-xml/
[4] https://www.geeksforgeeks.org/difference-between-json-and-xml/